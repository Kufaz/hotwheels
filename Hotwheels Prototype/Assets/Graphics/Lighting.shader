﻿Shader "Adventure Time/Lighting"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_LightMap("Lighting Map", 2D) = "gray" {}
		_LightNoise("Lighting Noise", 2D) = "gray" {}
    }
    SubShader
    {
        Tags { 
		"Queue" = "Geometry" 
		"RenderType"="Opaque" 
		"LightMode" = "ForwardBase"
		}

		Cull Back
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma multi_compile_fwdbase
            #pragma multi_compile_fog
			#pragma target 5.0

            #include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

            struct appdata
            {
                float4 pos : POSITION;	
				float3 normal: NORMAL;
                float2 uv : TEXCOORD0;
			
            };

            struct v2f
            {
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
				half3 normal : TEXCOORD1;			
				float4 posWorld : TEXCOORD2;
                UNITY_FOG_COORDS(3)
				LIGHTING_COORDS(4, 5)
                
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _LightMap;
			sampler2D _LightNoise;
			float4 _LightNoise_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.pos);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.posWorld = mul(unity_ObjectToWorld, v.pos);
                UNITY_TRANSFER_FOG(o,o.pos);
				TRANSFER_VERTEX_TO_FRAGMENT(o);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {	
				// light noise
				fixed4 lightNoise = tex2D(_LightNoise, i.uv);
				// light and view ramps
				float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
				float3 normalDir = normalize(i.normal*lightNoise);
				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float lightRamp = (dot(normalDir, lightDir)*.5)+.5;
				lightRamp = saturate(lightRamp * LIGHT_ATTENUATION(i));
				float viewRamp = saturate(dot(normalDir, viewDir));
				// sample the lighting map
				float2 lightMapSample = float2(viewRamp, lightRamp);
				fixed4 lightMap = tex2D(_LightMap, lightMapSample);			
				// light color and ambient
				float4 light = _LightColor0 * (lightMap + unity_AmbientSky);		
                // sample the texture
                fixed4 tex = tex2D(_MainTex, i.uv);
				//apply lighting
				fixed4 finalColor = tex*(light * 2);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, finalColor);
                return finalColor;
            }
            ENDCG
        }
    }
	Fallback "Diffuse"
}
